import {Component} from '@angular/core';
import {Student} from '../student';
import {ActivatedRoute, Router} from '@angular/router';
import {StudentService} from '../student.service';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent {

  student!: Student;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private studentService: StudentService) {
    this.student = new Student();
  }

  onSubmit(): any {
    this.studentService.save(this.student).subscribe(() => this.gotoUserList());
  }

  gotoUserList(): any {
    this.router.navigate(['/students']);
  }

}
