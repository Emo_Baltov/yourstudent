package com.yourstudent.gateway.management.rest;

import com.yourstudent.client.school.StudentClient;
import com.yourstudent.common.dto.StudentDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/v1/student")
public class StudentManagementResource {

    private final StudentClient studentClient;

    public StudentManagementResource(StudentClient studentClient) {
        this.studentClient = studentClient;
    }

    @GetMapping("find/first-name/{first-name}/last-name/{last-name}")
    public ResponseEntity<List<StudentDTO>> findByFirstNameAndLastName(@PathVariable("first-name") String firstName, @PathVariable("last-name") String lastName) {
        return studentClient.findByFirstNameAndLastName(firstName, lastName);
    }

    @PostMapping("create")
    public ResponseEntity<StudentDTO> createStudent(@RequestBody @Valid StudentDTO studentDTO) {
        return new ResponseEntity(studentClient.createStudent(studentDTO), HttpStatus.OK);
    }

    @PostMapping("import")
    public ResponseEntity<StudentDTO> importStudents(@RequestPart("file") MultipartFile file) throws IOException {
        return new ResponseEntity(studentClient.importStudents(file), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<StudentDTO>> getStudents(){
        return studentClient.getAllStudents();
    }
}
