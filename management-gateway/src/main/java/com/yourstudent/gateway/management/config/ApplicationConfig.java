package com.yourstudent.gateway.management.config;

import com.yourstudent.client.school.StudentClient;
import com.yourstudent.common.config.RestTemplateConfiguration;
import com.yourstudent.common.config.SwaggerConfig;
import com.yourstudent.common.config.WebConfig;
import com.yourstudent.common.properties.ApplicationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        SwaggerConfig.class,
        ApplicationProperties.class,
        StudentClient.class,
        RestTemplateConfiguration.class,
        WebConfig.class
})
public class ApplicationConfig {

}
