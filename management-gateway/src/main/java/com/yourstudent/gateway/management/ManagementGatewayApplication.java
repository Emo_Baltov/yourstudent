package com.yourstudent.gateway.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManagementGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManagementGatewayApplication.class, args);
    }

}
