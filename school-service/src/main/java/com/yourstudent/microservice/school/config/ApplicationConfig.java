package com.yourstudent.microservice.school.config;

import com.yourstudent.common.config.SwaggerConfig;
import com.yourstudent.common.config.WebConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        SwaggerConfig.class,
        WebConfig.class
})
public class ApplicationConfig {

}
