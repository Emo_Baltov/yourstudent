package com.yourstudent.microservice.school.service;

import com.yourstudent.common.dto.StudentDTO;
import com.yourstudent.microservice.school.exception.NotCorrectFileException;
import com.yourstudent.microservice.school.model.Student;
import com.yourstudent.microservice.school.repository.StudentRepository;
import com.yourstudent.microservice.school.utils.MapperHelper;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentService {

    private Logger logger = LoggerFactory.getLogger(StudentService.class);

    private final StudentRepository studentRepository;
    private final MapperHelper mapperHelper;

    public StudentService(StudentRepository studentRepository, MapperHelper mapperHelper) {
        this.studentRepository = studentRepository;
        this.mapperHelper = mapperHelper;
    }

    public List<StudentDTO> findByFirstNameAndLastName(String firstName, String lastName) {
        return studentRepository.findByFirstNameAndLastName(firstName, lastName).stream().map(mapperHelper::toStudentDTO).collect(Collectors.toList());
    }

    public StudentDTO createStudent(StudentDTO studentDTO) {
        return mapperHelper.toStudentDTO(studentRepository.save(mapperHelper.toStudent(studentDTO)));
    }

    public String importStudents(MultipartFile file) {

        File tempFile;
        Workbook workbook;
        try {
            tempFile = File.createTempFile("temp", ".tpm");
            file.transferTo(tempFile);
            workbook = new XSSFWorkbook(tempFile);
        } catch (IOException e) {
            logger.error("Not correct file {}", file.getOriginalFilename());
            throw new NotCorrectFileException();
        } catch (InvalidFormatException e) {
            logger.error("Not correct format for file with name {}", file.getOriginalFilename());
            throw new NotCorrectFileException();
        }

        Sheet sheet = workbook.getSheetAt(0);

        for (Row row : sheet) {
            if (row.getRowNum() > 0) {
                Student student = new Student();
                student.setFirstName(row.getCell(0).getStringCellValue());
                student.setLastName(row.getCell(1).getStringCellValue());
                student.setDateOfBirth(row.getCell(2).getDateCellValue().toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate());
                studentRepository.save(student);
            }
        }
        return file.getOriginalFilename();
    }

    public List<StudentDTO> getAllStudents() {
        return studentRepository.findAll().stream().map(mapperHelper::toStudentDTO).collect(Collectors.toList());
    }
}
