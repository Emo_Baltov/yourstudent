package com.yourstudent.microservice.school.config;

import com.yourstudent.microservice.school.model.Student;
import com.yourstudent.microservice.school.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.LocalDate;

@Component
public class DataLoader {

    @Autowired
    private StudentRepository studentRepository;

    //method invoked during the startup
    @PostConstruct
    public void loadData() {
        String firstName = "FirstName";
        String secondName = "SecondName";
        LocalDate dateOfBirth = LocalDate.of(2021, 1,1);
        for (int i = 1; i <= 100; i++) {
            Student student = new Student();
            student.setFirstName(firstName + i);
            student.setLastName(secondName + i);
            student.setDateOfBirth(dateOfBirth.plusDays(i));
            studentRepository.save(student);
        }

    }

    //method invoked during the shutdown
    @PreDestroy
    public void removeData() {
        studentRepository.deleteAll();
    }
}