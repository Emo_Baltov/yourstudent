package com.yourstudent.microservice.school.utils;

import com.yourstudent.common.dto.StudentDTO;
import com.yourstudent.microservice.school.model.Student;
import org.springframework.stereotype.Component;

@Component
public class MapperHelper {

    public StudentDTO toStudentDTO(Student student) {
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setFirstName(student.getFirstName());
        studentDTO.setLastName(student.getLastName());
        studentDTO.setDateOfBirth(student.getDateOfBirth());
        return studentDTO;
    }

    public Student toStudent(StudentDTO studentDTO) {
        Student student = new Student();
        student.setFirstName(studentDTO.getFirstName());
        student.setLastName(studentDTO.getLastName());
        student.setDateOfBirth(studentDTO.getDateOfBirth());
        return student;
    }
}
