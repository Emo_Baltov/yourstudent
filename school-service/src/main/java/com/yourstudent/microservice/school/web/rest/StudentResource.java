package com.yourstudent.microservice.school.web.rest;

import com.yourstudent.common.dto.StudentDTO;
import com.yourstudent.microservice.school.service.StudentService;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/v1/student")
public class StudentResource {
    private final StudentService studentService;

    public StudentResource(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping()
    public ResponseEntity<List<StudentDTO>> getAllStudents() {
        return new ResponseEntity(studentService.getAllStudents(), HttpStatus.OK);
    }

    @GetMapping("find/first-name/{first-name}/last-name/{last-name}")
    public ResponseEntity<List<StudentDTO>> findByFirstNameAndLastName(@PathVariable("first-name") String firstName, @PathVariable("last-name") String lastName) {
        return new ResponseEntity(studentService.findByFirstNameAndLastName(firstName, lastName), HttpStatus.OK);
    }

    @PostMapping("create")
    public ResponseEntity<StudentDTO> createStudent(@RequestBody @Valid StudentDTO studentDTO) {
        return new ResponseEntity(studentService.createStudent(studentDTO), HttpStatus.OK);
    }

    @PostMapping("import")
    public ResponseEntity<String> importStudents(@RequestPart("file") MultipartFile file) throws IOException, InvalidFormatException {
        return new ResponseEntity(studentService.importStudents(file), HttpStatus.OK);
    }

}
