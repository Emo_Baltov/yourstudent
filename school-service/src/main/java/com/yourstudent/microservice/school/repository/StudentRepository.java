package com.yourstudent.microservice.school.repository;

import com.yourstudent.microservice.school.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, String> {

    List<Student> findByFirstNameAndLastName(String firstName, String lastName);
}
