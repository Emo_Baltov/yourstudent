# Spring Boot Student Management System using Gradle

# Requirements
- Gradle
- IntelliJ
- NodeGs

# Porject Structure
- Basic project structure,
- If the root has any src/ folder then delete it. No problem.

# Created Modules
- There are two four modules as two of them are microservices:
    1. 'common' is a library which can be used by all application modules)
    2. 'management-gateway' is a gateway that will connect the frontend to the backend via Rest calls
    3. 'school-service-client' is a client that will connect the Gateways to the BE services
    4. 'school-service' is the main service of the app

//to do: Add additional info how to run the project