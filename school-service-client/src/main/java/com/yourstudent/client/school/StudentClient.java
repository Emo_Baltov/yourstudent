package com.yourstudent.client.school;

import com.yourstudent.common.dto.StudentDTO;
import com.yourstudent.common.properties.ApplicationProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class StudentClient {

    private final RestTemplate restTemplate;
    private final String serviceUrl;

    public StudentClient(ApplicationProperties applicationProperties, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.serviceUrl = applicationProperties.getProperty("school-service.url");
    }

    public ResponseEntity<List<StudentDTO>> getAllStudents() {
        return restTemplate.exchange(serviceUrl + "api/v1/student", HttpMethod.GET,
                null, new ParameterizedTypeReference<List<StudentDTO>>() {
                });
    }

    public ResponseEntity<List<StudentDTO>> findByFirstNameAndLastName(String firstName, String lastName) {
        return restTemplate.exchange(serviceUrl + "/api/v1/student/find/first-name/" + firstName + "/last-name/" + lastName, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<StudentDTO>>() {
                });
    }

    public ResponseEntity<StudentDTO> createStudent(StudentDTO studentDTO) {
        return restTemplate.postForEntity(serviceUrl + "/api/v1/student/create", studentDTO, StudentDTO.class);
    }

    public ResponseEntity<String> importStudents(MultipartFile file) throws IOException {

        byte[] fileAsByteArray = file.getBytes();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
        String filename = file.getOriginalFilename();
        ContentDisposition contentDisposition = ContentDisposition.builder("form-data").name("file").filename(filename)
                .build();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
        fileMap.add("Content-type", "multipart/form-data");

        HttpEntity<byte[]> fileEntity = new HttpEntity<>(fileAsByteArray, fileMap);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileEntity);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        return restTemplate.exchange(
                serviceUrl + "/api/v1/student/import", HttpMethod.POST, requestEntity, String.class);

    }
}
